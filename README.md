Machine Learning
=============
Provided by Stanford via Coursera.

Learn about the most effective machine learning techniques, and gain practice implementing them and getting them to work for yourself.

## Each week has several branches to keep the folder clean when working on exercises.